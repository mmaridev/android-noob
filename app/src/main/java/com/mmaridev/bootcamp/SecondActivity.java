package com.mmaridev.bootcamp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
    public void changeText(View view) {
        TextView mTextView = (TextView)this.findViewById(R.id.hellotest);
        mTextView.setText("nice :-)");
    }

}