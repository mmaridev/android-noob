package com.mmaridev.bootcamp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void changeText(View view) {
        TextView mTextView = (TextView)this.findViewById(R.id.hellotest);
        mTextView.setText("nice :-)");
    }
    public void launchSecondActivity(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }
    public void launchWtfIsMyIp(View view) {
        Intent intent = new Intent(this, wtfismyip.class);
        startActivity(intent);
    }
    public void launchNTP(View view) {
        Intent intent = new Intent(this, NTP.class);
        startActivity(intent);
    }
    public void launchSD(View view) {
        Intent intent = new Intent(this, SDcard.class);
        startActivity(intent);
    }
    public void startCsv(View view) {
        Intent intent = new Intent(this, CsvTest.class);
        startActivity(intent);
    }
}