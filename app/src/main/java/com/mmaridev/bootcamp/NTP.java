package com.mmaridev.bootcamp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class NTP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ntp);
        InitTrueTime initializer = new InitTrueTime(this);
        initializer.execute();
        Date now = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS z");
        String systime = "System Time: " + dateFormat.format(now);
        TextView sysdate = this.findViewById(R.id.sysdate);
        sysdate.setText(systime);
    }

    public void parseNTP (View view) {
        Log.d("MYAPP", "Button pressed, starting NTP");
        InitTrueTime initializer = new InitTrueTime(this);
        initializer.execute();
        Date now = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS z");
        String systime = "System Time: " + dateFormat.format(now);
        TextView sysdate = this.findViewById(R.id.sysdate);
        sysdate.setText(systime);
    }

    public void endActivity(View view) {
        finish();
    }
}
