package com.mmaridev.bootcamp;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class CsvTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_csv_test);
    }

    public void WriteCsv(View view) {
        Toast.makeText(this, "Writing csv file", Toast.LENGTH_SHORT).show();
        try {
            File output = new File(Environment.getExternalStorageDirectory(), "bootcamp/out.csv");
            String outpath = output.getAbsolutePath();
            Log.d("MyAPP", outpath);
            Writer fw = new FileWriter(outpath);
            String[] entries = "first#second#third".split("#");
            CSVWriter writer = new CSVWriter(fw, ',');
            writer.writeNext(entries);
            writer.close();
        }
        catch (IOException e) {
            Log.e("MyAPP", "CSVWritingError", e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
