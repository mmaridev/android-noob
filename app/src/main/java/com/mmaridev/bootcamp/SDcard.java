package com.mmaridev.bootcamp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class SDcard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdcard);
    }

    public void askPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    public void createFileDirs(View view) {
        askPermission();
        File file = new File(Environment.getExternalStorageDirectory(), "bootcamp");
        Log.d("MYAPP", file.getAbsolutePath());
        TextView statuslabel = this.findViewById(R.id.status);
        if (file.mkdirs()) {statuslabel.setText("Directory created successfully.");}
        else {statuslabel.setText("Directory creation failed (maybe already exists).");}
    }

    public void writeFile(View view) {
        askPermission();
        TextView statuslabel = this.findViewById(R.id.status);
        try {
            OutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "bootcamp/test.txt"));
            OutputStreamWriter osw = new OutputStreamWriter(out);
            osw.write("Lorem ipsum sit dolor amet brevi manu.");
            osw.close();
            statuslabel.setText("Write OK");
        }
        catch (IOException e) {
            statuslabel.setText("Write failed: " + e.getMessage());
            Log.d("MYAPP", "Write exception", e);
        }
    }
}
