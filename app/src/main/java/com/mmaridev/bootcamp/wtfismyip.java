package com.mmaridev.bootcamp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class wtfismyip extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wtfismyip);
        TextView yourip = (TextView) wtfismyip.this.findViewById(R.id.yourip);
        yourip.setText("Gathering data...");
        try {
            okhttprequest();
        }
        catch (Exception e) {
            Log.e("MYAPP", "exception", e);
        }

    }

    public void getIpEvent(View view) {
        TextView yourip = (TextView) wtfismyip.this.findViewById(R.id.yourip);
        yourip.setText("Gathering data...");
        try {
            okhttprequest();
        }
        catch (Exception e) {
            Log.e("MYAPP", "exception", e);
        }
    }

    public void okhttprequest() throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder().url("https://wtfismyip.com/json").build();
        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String myResponse = response.body().string();
                wtfismyip.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("MYAPP", myResponse);
                        TextView yourip = (TextView) wtfismyip.this.findViewById(R.id.yourip);
                        try {
                            JSONObject answ = new JSONObject(myResponse);
                            String IP = answ.getString("YourFuckingIPAddress");
                            String rDNS = answ.getString("YourFuckingHostname");
                            String loc = answ.getString("YourFuckingLocation");
                            String ISP = answ.getString("YourFuckingISP");
                            String country = answ.getString("YourFuckingCountryCode");
                            String out = "IP: " + IP + "\nReverse DNS: " + rDNS + "\n" + ISP + "\n" + loc + " " + country;
                            yourip.setText(out);
                        }
                        catch (Exception e) {}
                    }
                });
            }
        });
    }
}
