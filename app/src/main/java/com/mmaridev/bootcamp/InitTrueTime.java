package com.mmaridev.bootcamp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.instacart.library.truetime.TrueTime;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InitTrueTime extends AsyncTask<Void, Void, Void> {

    private WeakReference rootRef;

    public InitTrueTime (Context context) {
        rootRef = new WeakReference<>(context);
    }

    protected Void doInBackground(Void... params) {
        Log.d("MYAPP", "NTP background job started");
        try {
            TrueTime.build().withNtpHost("0.pool.ntp.org").initialize();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("MyApp", "Exception when trying to get TrueTime", e);
        }
        Date ntpdate = TrueTime.now();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS z");
        final String ntptime = "NTP Server time: " + dateFormat.format(ntpdate);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AppCompatActivity context = (AppCompatActivity) rootRef.get();
                TextView ntplabel = context.findViewById(R.id.ntpdate);
                ntplabel.setText(ntptime);
            }
        });
        return null;
    }
}